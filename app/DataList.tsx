import { View, Text, Image, Button, Share } from 'react-native'

import styles from './styles'


export default function DataList({ data }) {
    return (
        data.map(elt =>
            <View key={elt.toString()}>
                <Text style={styles.titleImageStyle}>{elt.date + ' : ' + elt.title}</Text>
                <View style={styles.imageContainer}>
                    <Image source={{ uri: elt.url }} style={styles.imageStyle} />
                </View>
                <Text style={styles.textImageStyle}>{elt.explanation}</Text>
                <View style={styles.shareButtonStyle}>
                    <Button onPress={() => { Share.share({ message: elt.url }) }}
                            title="Share"
                            color="#841584"
                            accessibilityLabel="Learn more about this purple button"
                    />
                </View>
            </View >
        )
    )
}