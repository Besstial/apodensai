import { StyleSheet } from 'react-native'


const styles = StyleSheet.create({
    titleStyle: {
        display: 'flex',
        justifyContent: 'center',
        textAlign: 'center',
        fontSize: 40,
        margin: 10,
        fontWeight: 'bold',
    },
    textImageStyle: {
        display: 'flex',
        justifyContent: 'center',
        textAlign: 'justify',
        margin: 25
    },
    titleImageStyle: {
        display: 'flex',
        justifyContent: 'center',
        textAlign: 'center',
        margin: 10,
        fontSize: 20,
        fontWeight: 'bold',
    },
    bottomTextStyle: {
        display: 'flex',
        justifyContent: 'center',
        textAlign: 'center',
        fontWeight: 'bold',
        color: 'grey',
        margin: 10
    },
    inputTextStyle: {
        marginBottom: 10,
        marginHorizontal: 50,
        padding: 2,
        borderWidth: 1,
        textAlign: 'center',
        borderRadius: 10,
    },
    shareButtonStyle: {
        margin: 20,
        flex: 1,
    },
    doubleButtonStyle: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        margin: 10,
        flex: 1,
    },
    imageContainer: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageStyle: {
        width: 400,
        height: 400,
        borderRadius: 10,
    }
})

export default styles
