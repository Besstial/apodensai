import { useState, useEffect, useCallback } from 'react';
import { SafeAreaView, ScrollView, StatusBar, StyleSheet, Text, View, Button, Share, Image, Switch, ActivityIndicator, TextInput, Alert, Linking } from 'react-native';

import OpenURLButton from './OpenURLButton'
import DataList from './DataList'
import styles from './styles'


export default function App() {

  const supportedURL = "https://api.nasa.gov/"
  const [apiKey, setApiKey] = useState("DEMO_KEY")
  const [url, setUrl] = useState("https://api.nasa.gov/planetary/apod?api_key=" + apiKey)
  const [data, setData] = useState(undefined)
  const [count, setCount] = useState(0)
  const [isEnabled, setIsEnabled] = useState(false)

  useEffect(() => {
    fetch(url)
      .then((resp) => resp.json())
      .then((json) => { if (json.length == undefined) { setData([json]); } else { setData(json) } })
  }, [count]);

  return (
    <SafeAreaView>
        <StatusBar />
        <ScrollView>
            <Text style={styles.titleStyle}>APODENSAI</Text>
            <View style={styles.doubleButtonStyle}>
                <Button title="Today's image"
                        onPress={() => { setUrl("https://api.nasa.gov/planetary/apod?api_key=" + apiKey); setCount(count + 1) } }
                />
                <Button title="Random image"
                        onPress={() => { setUrl("https://api.nasa.gov/planetary/apod?api_key=" + apiKey + "&count=1"); setCount(count + 1) } }
                />
            </View>
            {
                data ? (
                    data[0].error ? (
                        <View>
                            <Text style={styles.textImageStyle}>You rush quota usage limit. Please enter an API key from APOD website: </Text>
                            <TextInput onChangeText={setApiKey} value={apiKey} style={styles.inputTextStyle}/>
                            <View style={styles.shareButtonStyle}>
                                <OpenURLButton url={supportedURL} title="Set an API key from the website."/>
                            </View>
                        </View>
                    ) : (
                        <DataList data={data} bool={isEnabled} />
                    )
                ) : (
                    <ActivityIndicator />
                )
            }
            <Text style={styles.bottomTextStyle}>APODENSAI 2024</Text>
        </ScrollView>
    </SafeAreaView >
  )
}

