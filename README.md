# Bienvenue sur APODENSAI 👋

APODENSAI est une application innovante qui vous permet de visualiser facilement l'image astronomique du jour fournie par l'API APOD de la NASA. Chaque jour, découvrez une nouvelle photo fascinante de l'univers, ou laissez-vous surprendre par une photo sélectionnée au hasard. Si une image vous touche particulièrement, partagez-la instantanément avec votre famille et vos amis grâce à notre fonction de partage intégrée. Explorez les merveilles de l'espace et restez connecté avec vos proches grâce à APODENSAI.

## Get started

1. Install dependencies

   ```bash
   npm install
   ```

2. Start the app

   ```bash
    npx expo start
   ```

## Publier une application sur Android

Pour pouvoir publier une application dans le Google Play Store, Google utilise un Certificat de clé d'importation. Avec Expo, une clé est crée par defaut lors build de votre application. Il faut utiliser cette clé pour signer chaque release afin que Google sache que vous êtes à l'origine des modifications.
Si lorsque vous publiez une mise à jour de votre application avec une clé différente, vous aurez un message comme celui-ci : "The Android App Bundle was signed with the wrong key". Dans ce cas de panique, vous pouvez changer cette clé dans la console du play store. Avec Expo, vous devez vous déplacer dans le menu des credentials avec rubrique Build Credentials et télécharger l'Android Keystore correspondant. Dans le zip téléchargé, vous trouverez votre keystore au format JKS et un mardown avec les informations relatives à votre keystore comme les mots de passes et l'alias. Ensuite, à la demande de Google il faut exporter votre nouvelle clé au format PEM. La commande a éxecuté est la suivante :
`keytool -export -rfc -keystore <keystore_file.jks> -alias <keystore_alias> -file <new_file.pem>`
La commande keytool vous demandera alors le mot de passe du keystore. Vous pouvez ensuite importer votre nouvelle clé au format PEM dans le menu Publier/Configuration/Signature d'application et la rublique Certificat de clé d'importation. Google vous enverra un mail du changement et sera effectif 3 jours après importation. Vous pouvez comparer les différentes empreintes du nouveau certificat pour vérifier que cela correspond bien avec le keystore sur Expo (Vous regardez le certificat public de votre clé d'importation privée).
